
const getters = {
  tag: state => state.tags.tag,
  language: state => state.common.language,
  isUpdatePassword: state => state.common.isUpdatePassword,
  website: state => state.common.website,
  userInfo: state => state.user.userInfo,
  cid: state => state.user.cid,
  passFlag: state => state.user.passFlag,
  colorName: state => state.common.colorName,
  themeName: state => state.common.themeName,
  isShade: state => state.common.isShade,
  isCollapse: state => state.common.isCollapse,
  keyCollapse: (state, getters) => getters.screen > 1 ? getters.isCollapse : false,
  screen: state => state.common.screen,
  isLock: state => state.common.isLock,
  isFullScren: state => state.common.isFullScren,
  isMenu: state => state.common.isMenu,
  lockPasswd: state => state.common.lockPasswd,
  menuBtnPer: state => state.common.menuBtnPer,
  tagList: state => state.tags.tagList,
  tagWel: state => state.tags.tagWel,
  token: state => state.user.token,
  tokenType: state => state.user.tokenType,
  expiresIn: state => state.user.expiresIn,
  rolesId: state => state.user.rolesId,
  tenantCode: state => state.user.tenantCode,
  logo: state => state.user.logo,
  permission: state => state.user.permission,
  menuId: state => state.user.menuId,
  menu: state => state.user.menu,
  menuAll: state => state.user.menuAll,
  isShowTenantCode: (state, getters) => {
    if (!getters.website.isTenant){
      return false;
    }

    return state.user.rolesId.includes(getters.website.rootRoleId);
    // return false;
  },
  logsList: state => state.logs.logsList,
  logsLen: state => state.logs.logsList.length || 0,
  logsFlag: (state, getters) => getters.logsLen === 0,
  isDebug: (state, getters) => {
    return ['15011071226', '13132026510', 'mazq', 'lipw', 'lpr', 'fangyb'].includes(getters.userInfo.username)
  }
  // ,isOnely (state, getters) {
  //   return ![10, 22, 18].includes(getters.roles);
  // },
  // quxianFlag: (state, getters) => {
  //   if ([16].includes(getters.roles)) return false
  //   if ([19].includes(getters.roles)) return true
  //   if ([18].includes(getters.roles)) return false
  //   if ([22].includes(getters.roles)) return false
  //   return ![11, 12, 13, 14, 20, 21].includes(getters.roles);
  // },
  // jiedaoFlag: (state, getters) => {
  //   if ([18].includes(getters.roles)) return false
  //   if (getters.quxianFlag) {
  //     return true
  //   } else {
  //     return [11, 22].includes(getters.roles);
  //   }
  // },
  // shequFlag: (state, getters) => {
  //   if ([20].includes(getters.roles)) return true
  //   if ([18].includes(getters.roles)) return true
  //   if (getters.jiedaoFlag) {
  //     return true
  //   } else {
  //     return [11, 12].includes(getters.roles);
  //   }
  // },
  // xiaoquFlag: (state, getters) => {
  //   if ([21].includes(getters.roles)) return true
  //   if (getters.shequFlag) {
  //     return true
  //   } else {
  //     return [11, 12, 13].includes(getters.roles);
  //   }
  // },
  // quxianItem: (state, getters) => {
  //   if (!getters.quxianFlag) return []
  //   if (getters.jiedaoFlag) {
  //     return ['jiedao', 'shequ', 'xiaoqu']
  //   }
  // },
  // jiedaoItem: (state, getters) => {
  //   if (!getters.jiedaoFlag) return []
  //   if (!getters.quxianFlag && getters.jiedaoFlag) {
  //     return ['shequ', 'xiaoqu']
  //   }
  // },
  // shequItem: (state, getters) => {
  //   if (!getters.shequFlag) return []
  //   if (!getters.quxianFlag && !getters.jiedaoFlag && getters.shequFlag) {
  //     return ['xiaoqu']
  //   }
  // },
  // xiaoquItem: (state, getters) => {
  //   if (!getters.xiaoquFlag) return []
  //   if (!getters.quxianFlag && !getters.jiedaoFlag && !getters.shequFlag && getters.xiaoquFlag) {
  //     return []
  //   }
  // }
}
export default getters