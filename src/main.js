import Vue from 'vue';
import Bmob from "hydrogen-js-sdk";
import axios from './router/axios';
import VueAxios from 'vue-axios';
import App from './App';
import router from './router/router';
import './permission'; // 权限
import './error'; // 日志
import './cache';//页面缓冲
import store from './store';
import { loadStyle } from './util/util'
import * as urls from '@/config/env';
import crudCommon from '@/mixins/crud'
import Element from 'element-ui';

import {
  iconfontUrl,
  iconfontVersion
} from '@/config/env';
import i18n from './lang' // Internationalization
import './styles/common.scss';
import basicBlock from './components/basic-block/main'
import basicContainer from './components/basic-container/main'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import AvueMap from 'avue-plugin-map'
Vue.use(AvueMap);
Vue.use(Viewer)
Viewer.setDefaults({
  Options: { 'inline': true, 'button': true, 'navbar': true, 'title': true, 'toolbar': true, 'tooltip': true, 'movable': true, 'zoomable': true, 'rotatable': true, 'scalable': true, 'transition': true, 'fullscreen': true, 'keyboard': true, 'url': 'data-source' }
})
Vue.use(router)
Vue.use(VueAxios, axios)
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(window.AVUE, {
  i18n: (key, value) => i18n.t(key, value)
})
//注册全局容器
Vue.component('basicContainer', basicContainer)
Vue.component('basicBlock', basicBlock)
window.$crudCommon = crudCommon
// 加载相关url地址
Object.keys(urls).forEach(key => {
  Vue.prototype[key] = urls[key];
})

// 动态加载阿里云字体库
iconfontVersion.forEach(ele => {
  loadStyle(iconfontUrl.replace('$key', ele));
})
/* eslint-disable */
Vue.prototype.desensitization = (str = '') => {
  return str.replace(/^(.{6})(?:\d+)(.{4})$/, "\$1****\$2")
}
/* eslint-disable */
Vue.prototype.phonetization = (str = '') => {
  if (null != str && str != undefined) {
    var pat = /(\d{3})\d*(\d{4})/;
    return str.replace(pat, '$1****$2');
  } else {
    return "";
  }
}
Bmob.initialize("8670f16d28a1ef80", "123456");
Vue.prototype.Bmob = Bmob
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')