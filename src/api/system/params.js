
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.system+"/core";

export const effectAll = () => request.get(url + '/param/effectAll')
export const effect = (code) => request.get(url + '/param/takeEffect', {
  params: {
    code
  }
})
export const update = (id, data) => request({
  url: url + '/param/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
})
export const add = (data) => request({
  url: url + '/param/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const del = (id) => request.delete(url + '/param/delete', {
  params: {
    ids: id
  }
})
export const list = (data) => {
  return request({
    url: url + '/param/list',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: data
  })
}