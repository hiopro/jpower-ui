
import request from '@/router/axios';
import { urllist } from '@/config/env';
import store from '@/store/';
const url = urllist.system+"/core";
const listBut = (data) => request.get(url + '/function/listBut', {
  params: data
})

export function isShowButton (id, code) {
  return new Promise(function (resolve) {
    if(store.getters.menuBtnPer[id]){
      if (store.getters.menuBtnPer[id].includes(code)){
        resolve(true);
      }else {
        resolve(false);
      }
    }else {
      listBut({
        id: id
      }).then(res => {
        let flag = false;

        let codes = [];
        res.data.data.forEach(ele => {
          codes.push(ele.code);
          if (ele.code === code) {
            flag = true
          }
        });

        let session = store.getters.menuBtnPer;
        session[id] = codes;
        store.commit("SET_MENU_BTN_PER", session)

        resolve(flag);
      })
    }

  });



}
