
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.file

export const detail = (id) => request.get(url + '/core/file/get', {
    params: {
        id: id
    }
})
export const del = (id) => request.delete(url + '/core/file/delete', {
    params: {
        ids: id
    }
})
export const update = (id, data) => request({
    url: url + '/core/file/update',
    method: 'put',
    meta: {
        isSerialize: true
    },
    data: data
})
export const list = (data) => request.get(url + '/core/file/listPage', {
    params: data
})